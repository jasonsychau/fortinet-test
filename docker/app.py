from flask import Flask, request, render_template, abort, jsonify
from flask_caching import Cache
import requests
import json
import os

app = Flask(__name__)
cache = Cache(app, config={'CACHE_TYPE':'simple','CACHE_DEFAULT_TIMEOUT':86400})
key = os.environ.get('VT_KEY')

@app.route('/')
def open():
	return render_template('form.html')

@app.route('/fetch_data', methods=['POST'])
def run():
	data = json.loads(request.form['hashCodes'])

	response_data = [requestAnalysis(code) for code in data]

	response_data = [requestResults(res['md5']) if 'error' not in res.keys() else res for res in response_data]

	for (id,rep) in enumerate(response_data):
		if ('error' in rep.keys() or rep['response_code']==0):
			cache.delete_memoized(sendRequest,data[id])

	return jsonify(response_data)

def requestAnalysis(hashCode):
	next_request = hashCode
	result = sendRequest(hashCode)
	if ('error' in result.keys()):
		cache.delete_memoized(sendRequest,hashCode)
	return result
@cache.memoize()
def sendRequest(hashCode):
	try:
		response = requests.post("https://www.virustotal.com/vtapi/v2/file/scan", data={"apikey":key,"file":hashCode})
		if (response.status_code!=200):
            		return makeError("status code {}".format(response.status_code))
		elif (response.headers['content-type']!='application/json'):
			return makeError("unexpected response type {}".format(response.headers['content-type']))
		else:
			return response.json()
	except:
		return makeError("unexpected response type {}".format(response.headers['content-type']))

def requestResults(hashVal):
	next_analysis = hashVal
	result = sendForResults(hashVal)
	if ('error' in result.keys() or result['response_code']!=1):
		cache.delete_memoized(sendForResults,hashVal)
	return result
@cache.memoize()
def sendForResults(hashVal):
	try:
		response = requests.get("https://www.virustotal.com/vtapi/v2/file/report?apikey={}&resource={}".format(key,hashVal))
		if (response.status_code!=200):
			return makeError("status code {}".format(response.status_code))
		elif (response.headers['content-type']!='application/json'):
			return makeError("unexpected response type {}".format(response.headers['content-type']))
		else:
			return response.json()
	except:
		return makeError("found get request error")

def makeError(message):
	return { "error": message }

if __name__=='__main__':
	app.run()
