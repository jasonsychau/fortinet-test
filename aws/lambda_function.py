import json
import os
import requests
import psycopg2

key = os.environ.get('VT_KEY')
conn = psycopg2.connect(os.environ.get('DB_CONN'))

def lambda_handler(event, context):
	cur = conn.cursor()
	data = event['hashCodes']
	if (len(data)==0):
		return [];

	query = "select hashCode,md5 from AnalysisRequest where hashCode in ('{}'".format(data[0])
	for code in data[1:]:
		query += ',\'{}\''.format(code)
	query += ');'
	cur.execute(query)
	next = cur.fetchone()
	while (next!=None):
		data = list(map(lambda code: code if code!=next[0] else {'md5':next[1],'hashCode':code,'fromDB':1},data))
		next = cur.fetchone()
	
	data = [requestAnalysis(code) if (type(code)==str) else code for code in data]
	count = 0
	query = 'insert into AnalysisRequest (hashCode,md5) values '
	for (id,resp) in enumerate(data):
		if ('error' not in resp.keys() and 'fromDB' not in resp.keys()):
			count += 1
			query += '(\'{}\',\'{}\'),'.format(event['hashCodes'][id],resp['md5'])
	if (count>0):
		query = '{};'.format(query[:-1])
		cur.execute(query)
	
	query = 'select md5,fortinetName,count,scandate from ScanResult where md5 in ('
	count = 0
	for rId in data:
		if ('error' not in rId.keys()):
			count += 1
			query += '\'{}\','.format(rId['md5'])
	if (count>0):
		query = '{});'.format(query[:-1])
		cur.execute(query)
		next = cur.fetchone()
		while (next!=None):
			data = list(map(lambda rec: rec if ('error' in rec.keys() or rec['md5']!=next[0]) else {'md5':next[0],'response_code':1,'scans':{'Fortinet':{'result':next[1]}},'positives':next[2],'scan_date':next[3],'fromDB2':1}, data))
			next = cur.fetchone()

	data = [requestResults(rec['md5']) if ('error' not in rec.keys() and 'fromDB2' not in rec.keys()) else rec for rec in data]

	iQuery = 'insert into ScanResult (md5,fortinetName,count,scandate) values'
	iCount = 0
	rQuery = 'delete from AnalysisRequest where md5 in ('
	rCount = 0
	for rec in data:
		if ('error' not in rec.keys() and 'fromDB2' not in rec.keys()):
			if (rec['response_code']==1):
				iCount += 1
				iQuery += '(\'{}\',\'{}\',{},\'{}\'),'.format(rec['md5'],rec['scans']['Fortinet']['result'] if ('Fortinet' in rec['scans'].keys() and rec['scans']['Fortinet']['result'])!=None else 'null',rec['positives'],rec['scan_date'])
			elif (rec['response_code']==0):
				rCount += 1
				rQuery += '\'{}\','.format(rec['md5'])
	if (rCount>0):
		cur.execute('{});'.format(rQuery[:-1]))
	if (iCount>0):
		cur.execute('{};'.format(iQuery[:-1]))

	conn.commit()
	cur.close()

	return data

def makeError(message):
    return { 'error': message }

def requestAnalysis(hashCode):
	try:
		response = requests.post("https://www.virustotal.com/vtapi/v2/file/scan", data={"apikey":key,"file":hashCode})
		if (response.status_code!=200):
			return makeError("status code {}".format(response.status_code))
		elif (response.headers['content-type']!='application/json'):
			return makeError("unexpected response type {}".format(response.headers['content-type']))
		else:
			return response.json()
	except:
		return makeError("found post request error")

def requestResults(hashVal):
	try:
		response = requests.get("https://www.virustotal.com/vtapi/v2/file/report?apikey={}&resource={}".format(key,hashVal))
		if (response.status_code!=200):
			return makeError("status code {}".format(response.status_code))
		elif (response.headers['content-type']!='application/json'):
			return makeError("unexpected response type {}".format(response.headers['content-type']))
		else:
			return response.json()
	except:
		return makeError("found get request error")

