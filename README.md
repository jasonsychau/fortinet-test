# fortinet-test

## code layout

The webpage is built with HTML/CSS/Javascript, and the backend is built with Python.

## Website

The application is available at [https://fortinet-test.jasonsychau.com](https://fortinet-test.jasonsychau.com). I have deployed on with AWS and PostgreSQL. The database is for saving api results, so one is able to scan larger files in time. UI instructions are to described on the webpage. I think that system requirements is only a modern browser. I used Python for the backend, and the file is in the aws folder in this repo.

## Dockerized

I published a docker container to DockerHub (and included the code files in this repo). I have used Flask as web framework, Flask-Caching to cache results, and requests for making internet requests. If you install docker

```
sudo apt install docker.io
docker login
```

and pull image

```
docker pull jasonsychau/fortinet_test:latest
```

you can run a server on your machine

```
docker run -p 5000:5000 --network=host jasonsychau/fortinet_test
```

and visit the IP address and port displayed from web browser.

Alternatively, you can build the image from the files in this repo

```
cd <path/to/repo/docker>
docker build -t fortinet_test:latest .
```

and visit the IP address and port that is displayed.

To stop the container, 

```
docker ps (find the container name)
docker stop <CONTAINER_NAME>
docker rm <CONTAINER_NAME>
```

Then, you can remove the image with

```
docker image rm fortinet_test

docker image rm jasonsychau/fortinet_test
```

from your computer.

The server is in app.py, and the front-end is in templates/form.html.

## Issues

Issues are api quota and cost to deploy a container (I opted for a serverless design). The api quota is solved with caching.

## Assumptions

I assume that reports are expired in one day. If VirusTotal has a report that is more than one day old, I will not fetch that report but request another one.

For the application deployed on AWS, I assume that dataprocessing is done within 2 minutes. Anymore is not returning a result.

## Closing

That's it - thanks for tuning-in, and I hope to hear back from you.


Jason

P.S. My email is jasonsychau@gmail.com for contact follow-up.